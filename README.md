# arch-install

Please use at your own risk. This script is destructive and will wipe the designated drive for Arch installation with the following:

- BTRFS filesystem
- Encrypted volumes
- EFI boot
- Swap file
- Base Arch installation and configuration


**This installation does not (yet) include a Window Manager/Desktop Environment.**

## Requirements

This script requires:

1. Internet Connectivity (ethernet | wifi-menu)
2. EFI Boot

## Steps

1. Make a bootable Arch iso
2. Ensure network connectivity (use the wifi-menu command if you do not have ethernet)
3. Clone this repo (https://gitlab.com/greglaurent/arch-install)
4. Update resources/vars file with your configuration
5. Execute 'bash run'
6. Reboot

## Example vars File

Only update the variables that you need to update. The USER block contains sensitive information. Do not commit. Additionally, the drive is encrypted with the value set to ENCRYPT_PASSWD. DO NOT FORGET THIS PASSWORD.

Timezone is set by geolocation.

Default values:
- Keyboard: us layout
- Swap Size: 1 Gb
- Language: English

```
# SYSTEM 

DRIVE=/dev/sda
LANG=en_US.UTF-8
KEYMAP=us
HOSTNAME=arrakis
SWAP_SIZE=1G

# USERS

ENCRYPT_PASSWD=changeme 
ROOT_PASSWD=changeme
USERNAME=changeme
USER_PASSWD=changeme


# PATHS

EFI_NAME=EFI
CRYPT_NAME=cryptsystem
SYS_NAME=system
SWAPFILE=swapfile
```

## Notes

- DO NOT commit the vars file -- it will contain sensitive info
- Ensure you remember your password. The drive will be encrypted


